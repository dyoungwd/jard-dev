# [jardwd.space](https://jardwd.space)
**version 1.1**

A PWA built with [ember.js](https://emberjs.com/)

AddOns used:

**Service Worker Support**

```ember install ember-service-worker```

```ember-service-worker-asset-cache```

```ember-service-worker-cache-fallback```

Edit ember-cli-build.js and add the 'asset-cache' and 'esw-cache-fallback' sections


``` /* eslint-env node */
  const EmberApp = require('ember-cli/lib/broccoli/ember-app');
=======
```/* eslint-env node */
const EmberApp = require('ember-cli/lib/broccoli/ember-app');
>>>>>>> 9137d2f8288406752dd70b758f2f5dfdd8178c82

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    'asset-cache': {
      include: [
        'assets/**/*',
        'ember-welcome-page/images/*'
      ]
    },
    'esw-cache-fallback': {
      patterns: [ '/' ],
      version: '1' // Changing the version will bust the cache
    }
  });

  return app.toTree();
};
```


**Install Fastboot**

``` ember install ember-cli-fastboot ```

For a PWA, a manifest is required so we use the ember-web-app addon

```ember install ember-web-app```


**Styling**

<del>For styling, we use [ember paper](https://miguelcobain.github.io/ember-paper/)</del>

<del>```ember install ember-paper```</del>

For info on how to use ember check out the [ember.js docs](https://emberjs.com/learn/)

PWA ember.js guided can be found [here](https://madhatted.com/2017/6/16/building-a-progressive-web-app-with-ember)

**CHANGELOG**

v1.1 -
 Removed ember paper
 Added Flexgrid for layout
 Using inline CSS
 
v1.0



